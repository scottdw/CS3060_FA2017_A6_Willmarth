%Write a recursive predicate findFlight/2 that tells us when we can travel by plane between two towns.

nonStopFlight(houston, toledo).
nonStopFlight(toledo, detroit).
nonStopFlight(detroit, sanFrancisco).
nonStopFlight(sanFrancisco, columbus).
nonStopFlight(columbus, philadelphia).
nonStopFlight(philadelphia, pittsburgh).
nonStopFlight(pittsburgh, cleveland).

%You can travel between two towns when you can get from point A to point B (transitive property).
%you can get from toledo to detroit, but you cannot go from detroit to toledo
%houston can get to any town
%cleveland can't go to any town


findFlight(X,Y) :- nonStopFlight(X,Y).
findFlight(X,Y) :- nonStopFlight(X,Z), findFlight(Z,Y).

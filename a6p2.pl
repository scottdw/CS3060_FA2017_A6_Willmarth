smaller(a,b).
smaller(b,c).
smaller(c,d).
smaller(d,e).
smaller(e,f).
smaller(f,g).
smaller(g,h).
smaller(h,i).
smaller(i,j).
smaller(j,k).
smaller(k,l).
smaller(l,m).
smaller(m,n).
smaller(n,o).
smaller(o,p).
smaller(p,q).
smaller(q,r).
smaller(r,s).
smaller(s,t).
smaller(t,u).
smaller(u,v).
smaller(v,w).
smaller(w,x).
smaller(x,y).
smaller(y,z).

infer(X,Y) :- smaller(X,Y).
infer(X,Y) :- smaller(X,Z), infer(Z,Y).

min([X], X).
min([X,Y|Rest], Min) :- infer(X,Y), min([X|Rest], Min).
min([X,Y|Rest], Min) :- infer(Y,X), min([Y|Rest], Min).

1. To run problem 1, call reverse([any, list, here], R).
Ex. 
reverse([a,b,c], R). -> R = [c, b, a].
reverse([], R). -> R = [].
reverse([], []). -> true.

2. To run problem 2, call min([l,i,s,t], R).
Ex.
min([a,b,c], R). -> R = a
min([q,z,b,c,e], R). -> R = b

3. To run problem 3, call findFlight(City1, City2).
Ex. 
findFlight(houston, toledo). -> true
findFlight(houston, cleveland). -> true
findFlight(cleveland, columbus). -> false
findFlight(columbus, houston). -> false